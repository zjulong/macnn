import  torch
from    torch import nn
from    torchvision import datasets, transforms
from    torch.utils.data import DataLoader
from    torch.nn import functional as F

from    matplotlib import pyplot as plt
import  visdom
from    mconv import MConv2d
import  argparse
import  numpy as np

class LeNet(nn.Module):
	def __init__(self):
		super(LeNet, self).__init__()
		self.conv1 = nn.Conv2d(3, 6, 5)
		self.conv2 = nn.Conv2d(6, 16, 5)
		self.fc1 = nn.Linear(16 * 5 * 5, 200)
		self.fc2 = nn.Linear(200, 10)

	def forward(self, x):
		# [b, 3, 32, 32]
		# print('x', x.size())

		out = F.relu(self.conv1(x))
		out = F.max_pool2d(out, 2)
		# [6, 14, 14]
		# print('conv1', out.size())

		out = F.relu(self.conv2(out))
		out = F.max_pool2d(out, 2)
		# [16, 5, 5]
		# print('conv2', out.size())

		out = out.view(out.size(0), -1)
		out = F.relu(self.fc1(out))
		out = self.fc2(out)
		return out


class MLeNet(nn.Module):
	def __init__(self):
		super(MLeNet, self).__init__()
		self.conv1 = MConv2d(32, 3, 6, 5)
		self.conv2 = MConv2d(14, 6, 16, 5)
		self.fc1 = nn.Linear(16 * 5 * 5, 200)
		self.fc2 = nn.Linear(200, 10)

	def forward(self, x):
		# print('x', x.size())
		out = F.relu(self.conv1(x))
		# print('conv1', out.size())
		out = F.max_pool2d(out, 2)
		out = F.relu(self.conv2(out))
		# print('conv2', out.size())
		out = F.max_pool2d(out, 2)
		out = out.view(out.size(0), -1)
		out = F.relu(self.fc1(out))
		out = self.fc2(out)
		return out


def main():
	args  = argparse.ArgumentParser()
	args.add_argument('-b', action='store_true', help='set to use basic cnn unit')
	args = args.parse_args()

	batchsz = 20

	db = datasets.CIFAR10('cifar10', train=True, download=True,
	                    transform=transforms.Compose([
		                    transforms.ToTensor(),
		                    transforms.Normalize((0.5,), (0.5,))
	                    ])
	                    )
	db_loader = DataLoader(db, batch_size=batchsz, shuffle=False)

	db_test = datasets.CIFAR10('cifar10', train=False, download=True,
	                    transform=transforms.Compose([
		                    transforms.ToTensor(),
		                    transforms.Normalize((0.5,), (0.5,))
	                    ])
	                    )
	db_loader_test = DataLoader(db_test, batch_size=batchsz, shuffle=True)

	device = torch.device('cuda')
	if args.b: # use basic cnn
		lenet = LeNet().to(device)
	else:
		lenet = MLeNet().to(device)
	print(lenet)
	criteon = nn.CrossEntropyLoss()
	optimizer = torch.optim.Adam(lenet.parameters(), lr=1e-3)

	# interactive plotting
	vis = visdom.Visdom()
	# vis.scatter(torch.tensor([[0,0]]), win='acc cnn',
	# 				            opts={'caption':'acc-cnn','markercolor':np.array([[255,0,0]])})
	# vis.scatter(torch.tensor([[0,0]]), win='acc mcnn',
	# 				            opts={'caption':'acc-cnn','markercolor':np.array([[255,0,0]])})

	for epoch in range(10000):

		for step, batch in enumerate(db_loader):
			# if step * batchsz > 5000:
			# 	break
			# x: [b, 1, 28, 28]
			# y: [10]
			x, y = batch
			x = x.to(device)
			y = y.to(device)

			pred = lenet(x)

			loss = criteon(pred, y)
			optimizer.zero_grad()
			loss.backward()
			# nn.utils.clip_grad_norm_(lenet.parameters(), 10)
			optimizer.step()


			if step % 5000 == 0:
				total_counter = total_right = 0

				for batch_test in db_loader_test:
					x, y = batch_test
					x = x.to(device)
					y = y.to(device)

					pred = lenet(x)
					# [b, class] => [b]
					right = torch.argmax(pred, dim=1)
					# to numpy
					right = torch.eq(right, y).sum().item()

					total_right += right
					total_counter += y.size(0)

				acc = total_right / total_counter


				# # [b, 1, imgsz, imgsz]
				# img0 = x.mul(0.5).add(0.5)
				# img0 = F.upsample(img0, scale_factor=2)
				# vis.images(img0, nrow=5, win='initial image', env='ntm2d')
				# vis.text('acc:%.5f'%acc, win='acc', env='ntm2d')

				if args.b :
					vis.scatter(torch.tensor([[epoch,acc]]), win='acc cnn', update='append',
					            opts={'markercolor':np.array([[0,255,0]])})
				else:
					vis.scatter(torch.tensor([[epoch,acc]]), win='acc mcnn', update='append',
					            opts={'markercolor':np.array([[0,255,0]])})

				print(epoch, step, loss.item(), acc)


if __name__ == '__main__':
	main()
